/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.util.module.versioning.maven.plugin;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/**
 * <p>The PrintVersionMojo prints the current
 * maven version of the project.</p>
 * 
 * <p>The command will not print the actual pom.xml
 * file content, it will print the "calculated" version.</p>
 * 
 * <p>I.e. if your pom.xml contains ${revision} in the version
 * tag and you have a property in the pom which replaces
 * ${revision} with SNAPSHOT, the version printed will contain
 * SNAPSHOT.</p>
 *
 * @author Mikael Ahlberg
 */
@Mojo(name = "print-version")
public class PrintVersionMojo extends AbstractMojo {
    
    @Parameter(defaultValue = "${project.version}", required = true, readonly = true)
    private String projectVersion;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info("Project version is: " + projectVersion);
    }
}
