/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.util.module.versioning.maven.plugin;

import java.util.Set;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/**
 * <p>The PrintDependencyVersionMojo prints the version
 * of the provided dependency in the maven project.</p>
 * 
 * <p>The result is printed on the format:
 * 'Dependency version is: X'</p>
 * 
 * <p>If the dependency can not be found, the following
 * statement is printed:
 * 'Dependency not found: groupId:artifactId'</p>
 *
 * @author Mikael Ahlberg
 */
@Mojo(name = "print-dependency-version")
public class PrintDependencyVersionMojo extends AbstractMojo {
    
    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    private MavenProject mavenProject;
    
    @Parameter(property = "groupId", required = true)
    private String groupId;
    
    @Parameter(property = "artifactId", required = true)
    private String artifactId;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        @SuppressWarnings("unchecked")
        Set<Artifact> dependencyArtifacts = mavenProject.getDependencyArtifacts();
        
        for (Artifact artifact : dependencyArtifacts) {
            if (isMatchingArtifact(artifact)) {
                getLog().info("Dependency version is: " + artifact.getVersion());
                
                return;
            }
        }
        
        getLog().info("Dependency not found: " + groupId + ":" + artifactId);
    }
    
    private boolean isMatchingArtifact(Artifact artifact) {
        return artifact.getGroupId().equals(groupId) && artifact.getArtifactId().equals(artifactId);
    }
}
