/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.util.module.versioning.maven.plugin.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>The CalculateNewVersion is responsible for calculating
 * the new version. It only supports versions on the following
 * formats: 1.0.0 or 1.0.0.M1</p>
 *
 * @author Mikael Ahlberg
 */
public class CalculateNewVersion {

    private Pattern customPattern = Pattern.compile("([0-9]+.[0-9]+.[0-9]+.[A-Za-z]+)([0-9]+)");
    private Pattern regularPattern = Pattern.compile("([0-9]+.[0-9]+.)([0-9]+)");

    /**
     * <p>Increases the provided version, one patch version step or one
     * milestone version step.</p>
     *
     * @param currentVersion is the version to increase one minor step
     * @return an updated version string
     */
    public String calculate(String currentVersion) {
        String newVersion = null;

        if ((newVersion = createNewVersion(customPattern.matcher(currentVersion))) != null) {
            return newVersion;
        }

        if ((newVersion = createNewVersion(regularPattern.matcher(currentVersion))) != null) {
            return newVersion;
        }

        throw new RuntimeException("Version did not match pattern examples 1.0.0.M1 or 1.0.0 or 1.0.0.RC1");
    }

    private String createNewVersion(Matcher matcher) {
        if (matcher.matches()) {
            return matcher.group(1) + increaseNumber(matcher.group(2));
        }

        return null;
    }

    private String increaseNumber(String numberAsString) {
        int number = Integer.parseInt(numberAsString);

        return "" + ++number;
    }
}
