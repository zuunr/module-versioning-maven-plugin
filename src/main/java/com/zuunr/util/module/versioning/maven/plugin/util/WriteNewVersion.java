/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.util.module.versioning.maven.plugin.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>The WriteNewVersion is responsible for reading, updating
 * and writing the new version of the maven module.</p>
 *
 * @author Mikael Ahlberg
 */
public class WriteNewVersion {

    /**
     * <p>Updates the pom.xml file with the new updated project version.</p>
     * 
     * @param currentVersion is the current project version
     * @param newProjectVersion is the new project version to write
     */
    public void updatePom(String currentVersion, String newProjectVersion) {
        ArrayList<String> pomContent = readPom();
        
        int index = findVersionRow(pomContent, currentVersion);
        updateVersion(index, pomContent, newProjectVersion, currentVersion);
        
        writePom(pomContent);
    }
    
    private void updateVersion(int index, ArrayList<String> pomContent, String newProjectVersion, String currentVersion) {
        String updatedLine = pomContent.get(index).replace("<version>" + currentVersion + "</version>", "<version>" + newProjectVersion + "</version>");
        
        pomContent.set(index, updatedLine);
    }

    private int findVersionRow(ArrayList<String> pomContent, String currentVersion) {
        int i = 0;
        while (i < pomContent.size()) {
            String line = pomContent.get(i);
            
            if (line.contains("<parent>")) {
                i = skipRowsToTag(pomContent, i, "</parent>");
                continue;
            }
            
            if (line.contains("<dependencies>")) {
                i = skipRowsToTag(pomContent, i, "</dependencies>");
                continue;
            }
            
            if (line.contains("<plugins>")) {
                i = skipRowsToTag(pomContent, i, "</plugins>");
                continue;
            }
            
            if (line.contains("<version>" + currentVersion + "</version>")) {
                return i;
            }
            
            i++;
        }
        
        throw new RuntimeException("Could not find version tag");
    }
    
    private int skipRowsToTag(ArrayList<String> pomContent, int startRow, String tag) {
        int i = startRow;
        while (i < pomContent.size()) {
            String line = pomContent.get(i);
            
            if (line.contains(tag)) {
                return i;
            }
            
            i++;
        }
        
        throw new RuntimeException("Could not locate end tag: " + tag);
    }

    private ArrayList<String> readPom() {
        try {
            return new ArrayList<>(Files.readAllLines(new File("pom.xml").toPath()));
        } catch (IOException e) {
            throw new RuntimeException("Could not locate and read pom.xml", e);
        }
    }
    
    private void writePom(List<String> pomContent) {
        try {
            Files.write(new File("pom.xml").toPath(), pomContent);
        } catch (IOException e) {
            throw new RuntimeException("Could not write pom.xml", e);
        }
    }
}
