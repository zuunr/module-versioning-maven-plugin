/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.util.module.versioning.maven.plugin;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import com.zuunr.util.module.versioning.maven.plugin.util.CalculateNewVersion;
import com.zuunr.util.module.versioning.maven.plugin.util.WriteNewVersion;

/**
 * <p>The IncreaseVersionMojo is responsible for calculating
 * a new maven version based on the current project version.</p>
 * 
 * <p>The result is printed on the format:
 * 'New project version is: X'</p>
 * 
 * <p>The version is increased one patch version, or one milestone version.</p>
 *
 * @author Mikael Ahlberg
 */
@Mojo(name = "increase-version")
public class IncreaseVersionMojo extends AbstractMojo {
    
    @Parameter(defaultValue = "${project.version}", required = true, readonly = true)
    private String projectVersion;
    
    @Parameter(property = "hasRevision")
    private boolean hasRevision;
    
    @Parameter(property = "revisionKeyName")
    private String revisionKeyName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        getLog().info("Current project version is: " + projectVersion);
        
        String[] splittedVersion = projectVersion.split("-");
        
        if (splittedVersion.length != 2) {
            throw new MojoExecutionException("Only supports versions containing exactly one '-' delimiter, e.g. 'X-SNAPSHOT'");
        }
        
        String newProjectVersion = new CalculateNewVersion().calculate(splittedVersion[0]);
        
        if (hasRevision) {
            String revision = "-${" + revisionKeyName + "}";
            newProjectVersion += revision;
            new WriteNewVersion().updatePom(splittedVersion[0] + revision, newProjectVersion);
        } else {
            newProjectVersion += "-" + splittedVersion[1];
            new WriteNewVersion().updatePom(projectVersion, newProjectVersion);
        }
        
        getLog().info("New project version is: " + newProjectVersion);
    }
}
