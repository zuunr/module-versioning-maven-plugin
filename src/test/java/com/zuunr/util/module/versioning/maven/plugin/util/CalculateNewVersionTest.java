/*
 * Copyright 2019 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.util.module.versioning.maven.plugin.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalculateNewVersionTest {

    private CalculateNewVersion calculateNewVersion = new CalculateNewVersion();

    @Test
    void givenPatchVersionShouldIncreaseWithOne() {
        assertEquals("1.2.4", calculateNewVersion.calculate("1.2.3"));
        assertEquals("1.0.10", calculateNewVersion.calculate("1.0.9"));
        assertEquals("1.0.100", calculateNewVersion.calculate("1.0.99"));
        assertEquals("1.0.110", calculateNewVersion.calculate("1.0.109"));
        assertEquals("1.0.1000", calculateNewVersion.calculate("1.0.999"));
        assertEquals("1.0.1010", calculateNewVersion.calculate("1.0.1009"));
        assertEquals("1.0.10020", calculateNewVersion.calculate("1.0.10019"));
    }

    @Test
    void givenMilestoneVersionShouldIncreaseWithOne() {
        assertEquals("1.2.3.M2", calculateNewVersion.calculate("1.2.3.M1"));
        assertEquals("1.0.109.M2", calculateNewVersion.calculate("1.0.109.M1"));
        assertEquals("1.0.109.M3", calculateNewVersion.calculate("1.0.109.M2"));
    }

    @Test
    void givenHotfixVersionShouldIncreaseHotfixWithOne() {
        assertEquals("1.0.0.hotfix2", calculateNewVersion.calculate("1.0.0.hotfix1"));
        assertEquals("112.2221.332.hotfix5", calculateNewVersion.calculate("112.2221.332.hotfix4"));
    }

    @Test
    void givenNonSupportingVersionShouldThrowException() {
        assertThrows(RuntimeException.class, () -> calculateNewVersion.calculate("1.2"));
        assertThrows(RuntimeException.class, () -> calculateNewVersion.calculate("A.B"));
        assertThrows(RuntimeException.class, () -> calculateNewVersion.calculate("A.B.C"));
        assertThrows(RuntimeException.class, () -> calculateNewVersion.calculate("A.B.C.MX"));
    }
}
