# module-versioning-maven-plugin

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr.util/module-versioning-maven-plugin.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=a:module-versioning-maven-plugin)

This is the module versioning maven plugin. It is used to update the patch, or milestone, version of a selected maven module. It can also print the current version, and the version of a selected dependency.

## Supported tags

* [`1.0.0-5de21cd`, (*5de21cd/pom.xml*)](https://bitbucket.org/zuunr/module-versioning-maven-plugin/src/5de21cd/pom.xml)

## Usage

To use this module, add this module to your maven command line:

```bash
mvn com.zuunr.util:module-versioning-maven-plugin:print-version

mvn com.zuunr.util:module-versioning-maven-plugin:increase-version

mvn com.zuunr.util:module-versioning-maven-plugin:print-dependency-version -DgroupId=dependency.group.id -DartifactId=my-artifact-id
```

## Configuration

### increase-version
The module has the following two properties which can be applied for the increase-version command:

* hasRevision: boolean
* revisionKeyName: string

The **-DhasRevision** is used when your version has a variable in your version tag, e.g. 1.0.0-${revision}. If set to true, the printed version will contain the ${revision} tag and not the calculated maven version.

The **-DrevisionKeyName** must be set if the **-DhasRevision** is set to true. This is the name to print inside the ${} tags, e.g. 1.0.0-${revisionKeyName}.

### print-dependency-version
The module has the following two properties which can be applied for the print-dependency-version command:

* groupId: string
* artifactId: string

The properties are used to determine which dependency to print the version for.
